from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from time import sleep
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
#from faker import Faker
import unittest, time, re, datetime
import warnings
import os
#from dotenv import load_dotenv


class Scenariotest(unittest.TestCase):
    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        s=Service(r'C:\Users\Admin\Documents\PVG\chromedriver.exe')
        self.driver = webdriver.Chrome(service=s)
        self.driver.implicitly_wait(5)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_Login(self):
        driver = self.driver
        #wait = WebDriverWait(driver, 20)
        driver.get("https://evermos.com/home/")
        driver.maximize_window()
        #step 1 Click button masuk
        driver.find_element(By.LINK_TEXT, "Masuk").click()
        time.sleep(2)
        #Step 2 Input Nomor telepon
        driver.find_element(By.XPATH, "//input[@value='']").click()
        driver.find_element(By.XPATH, "//input[@value='']").clear()
        driver.find_element(By.XPATH, "//input[@value='']").send_keys("6281223334444")
        time.sleep(2)
        # Step 3 input password
        driver.find_element(By.XPATH, "//div[@id='__layout']/div/div[2]/form/label[2]/span[2]/input").click()
        driver.find_element(By.XPATH, "//div[@id='__layout']/div/div[2]/form/label[2]/span[2]/input").clear()
        driver.find_element(By.XPATH, "//div[@id='__layout']/div/div[2]/form/label[2]/span[2]/input").send_keys("password")
        time.sleep(2)
        #driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        #Step 4 Click button Masuk
        button = driver.find_element(By.XPATH, "//button[@type='button']")
        driver.execute_script("arguments[0].click();", button)
        #driver.find_element(By.XPATH, "//button[@type='button']").click()
        time.sleep(2)
        #Step 4 click menu akun
        driver.find_element(By.LINK_TEXT, "Akun").click()
        time.sleep(2)
        try:
            VerifyOrder = self.driver.find_element(By.CLASS_NAME, "userBox__info__name").text
            assert "Zuma Deluxe zum9" in VerifyOrder
            print("Assertion is TRUE")

        except Exception as e:
            print("Assertion is FALSE", format(e))
        #step 6 see thank you page order
        driver.save_screenshot(r'C:\Users\Admin\Documents\evermos\ScenarioTestSCript\Screenshoot\Capture.png')

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
